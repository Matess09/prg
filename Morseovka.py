import unidecode

varOne = {
    'a':'.-',
    'b':'-...',
    'c':'-.-.',
    'd':'-..',
    'e':'.',
    'f':'..-.',
    'g':'--.',
    'h':'....',
    'i':'..',
    'j':'.---',
    'k':'-.-',
    'l':'.-..',
    'm':'--',
    'n':'-.',
    'o':'---',
    'p':'.--.',
    'q':'--.-',
    'r':'.-.',
    's':'...',
    't':'-',
    'u':'..-',
    'v':'...-',
    'w':'.--',
    'x':'-..-',
    'y':'-.--',
    'z':'--..'
}

varTwo = {
    '.-':'a',
    '-...':'b',
    '-.-.':'c',
    '-..':'d',
    '.':'e',
    '..-.':'f',
    '--.':'g',
    '....':'h',
    '..':'i',
    '.---':'j',
    '-.-':'k',
    '.-..':'l',
    '--':'m',
    '-.':'n',
    '---':'o',
    '.--.':'p',
    '--.-':'q',
    '.-.':'r',
    '...':'s',
    '-':'t',
    '..-':'u',
    '...-':'v',
    '.--':'w',
    '-..-':'x',
    '-.--':'y',
    '--..':'z'
    
}

choice = [1, 2]

var2 = []

print('PŘEKLADAČ MORSEOVKY A NAOPAK')
print('----------------------------------------------------------')
print('Nepoužívat velká písmena a číslice, pouze málá písmena z abecedy')
print('')
print('Pro překlad do morzeovky zadej 1, pokud naopak zadej 2.')
pick = input('Volba: ')

if int(pick) in choice:
    if int(pick) == 1:
        print('')
        print('----------------------------------------------------------')     
        print('PŘEKLAD Z LATINKY DO MORSEOVKY')
        print('')
        sent = input('Zadej větu: ')
        sent = unidecode.unidecode(sent)

        for item in sent:
            if item == ' ':
                var2.append(item)
            elif item in varOne.keys():
                item = varOne[item]
                var2.append(item)
            else:
                print('Nastala chyba')
                break
    else:
        print('')
        print('------------------------------------')
        print('PŘEKLAD Z MORSEOVKY DO LATINKY')
        print('Před každým morseovým kódem oddělit mezerou')
        print('Po dokončení slova oddělit slovo "_"')
        print('')
        sent = input('Zadej větu: ')

        sent = sent.split()

        for item in sent:
            if item == '_':
                var2.append(item)
            elif item in varTwo.keys():
                item = varTwo[item]
                var2.append(item)
            else:
                print('Nastala chyba')
                break
        
    print(*var2)
else:
    print('Nevhodný výběr')